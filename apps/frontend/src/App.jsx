import { useState } from 'react'
import './App.css'
import { useTranslation } from "react-i18next";
import { ButtonOpenChat } from './component/ButtonOpenChat/ButtonOpenChat'
import { ChatBot } from './view/ChatBot'

function App() {
  const [isOpen, setIsOpen] = useState(false)
  const { t } = useTranslation()
  const changeIsOpenHandler = () => {
    setIsOpen(state => !state)
  }

  return (
    <>
      <div>
        <p>{t("message.title")}</p>

        {
          isOpen ? (
            <ChatBot onClose={changeIsOpenHandler} />
          ) : <ButtonOpenChat onClick={changeIsOpenHandler} />
        }
      </div>
    </>
  )
}

export default App
