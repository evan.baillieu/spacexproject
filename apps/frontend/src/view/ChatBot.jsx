import { useEffect, useState } from 'react'
import CloseButton from '../assets/CloseButton.svg?react'
import SpaceBotIcon from '../assets/SpaceBOT.svg?react'
import { ZoneTextDefault } from '../component/ZoneTextDefault/ZoneTextDefault'
import { InputChat } from '../component/inputChat/inputChat'
import './ChatBot.scss'
import { MessagesList } from '../component/messagesList/MessagesList'
import { ButtonMenu } from '../component/Buttonmenu/ButtonMenu'
import { Menu } from '../component/Menu/Menu'
import { getIdConversation, setIdConversation } from '../helper/idConvertation'
import { useTranslation } from 'react-i18next'
import { sendMessageApi } from '../service/message'

export function ChatBot({ onClose }) {
    const [data, setData] = useState({})
    const [messages, setMessages] = useState([/*{
        "isYou": true,
        "message": "rwrwr"
    }, {
        "isYou": false,
        "message": "rwrwr"
    }, {
        "isYou": true,
        "message": "rwrwr"
    }, {
        "isYou": false,
        "message": "rwrwr"
    }*/])
    const [isOpenMenu, setIsOpenMenu] = useState(false)
    const { i18n } = useTranslation();

    const onMenuHandle = () => {
        setIsOpenMenu(state => !state);
    }

    useEffect(() => {
        if (data.message) {
            setMessages(state => [
                ...state,
                data
            ])
        }
    }, [data])

    const sendMessage = (message) => {
        const id = getIdConversation();
        const lang = i18n.language;

        sendMessageApi({ message, idConversation: id, lang }).then((data) => {
            if (data['id_conv']) {
                setIdConversation(data['id_conv']);
                setData({ message: data.message, isYou: false });
            }
        })

        setMessages(state => [
            ...state,
            {
                isYou: true,
                message
            }
        ])
    }
    return (
        <div data-testid="ChatBot" className="ChatBot">
            <div className="ChatBot__header">
                <div className='ChatBot__header__title'>
                    <h1>
                        <SpaceBotIcon />
                    </h1>
                </div>
                {isOpenMenu ? <Menu onSend={sendMessage} onClose={onMenuHandle} /> : <ButtonMenu onClick={onMenuHandle} />}
                <div className='ChatBot__header__CloseButton'>
                    <button onClick={onClose}>
                        <CloseButton width={"25px"} height={"25px"} />
                    </button>
                </div>
            </div>
            <div className="ChatBot__ZoneMessage" id="zoneMessage">
                {messages.length === 0 ? <ZoneTextDefault /> : <MessagesList messages={messages} />}
            </div>
            <div className="ChatBot__footer">
                <InputChat onSend={sendMessage} />
            </div>
        </div>
    )
}

