export const setIdConversation = (id) => {
  localStorage.setItem("idConvertation", id);
};

export const getIdConversation = () => {
  return localStorage.getItem("idConvertation");
};
