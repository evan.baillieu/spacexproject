export const LANGIT = {
  message: {
    title: "Benvenuto su SpaceBOT!",
    pargraphe1:
      "Il tuo compagno virtuale progettato specificamente per semplificare la tua esperienza rispondendo alle tue domande e guidandoti attraverso informazioni eccitanti legate alle attività spaziali di SpaceX.",
    pargraphe2:
      "Non è necessaria alcuna competenza tecnica per interagire con il nostro assistente intelligente. Basta inviare messaggi tramite il sistema di messaggistica integrato o selezionare un'opzione dal menu a sinistra e il chatbot fornirà risposte automatiche in base alle tue scelte.",
    pargraphe3:
      "Per rendere questa esperienza ancora più ricca, il nostro chatbot utilizza una API esterna specializzata in risposte basate sull'intelligenza artificiale, garantendo interazioni informative e amichevoli.",
    pargraphe4:
      "Esplora lo spazio senza sforzo con il nostro chatbot SpaceX, la tua guida virtuale dedicata alle meraviglie dell'esplorazione spaziale!",
  },
  input: {
    placeholder: "Fai la tua domanda...",
    lang: {
      fr: "Français",
      en: "English",
      it: "Italiano",
      es: "Español",
    },
  },
  menu: {
    title: "Non sai da dove cominciare?",
    question1: "Dammi informazioni sull'azienda SpaceX.",
    question2: "Cosa significa SpaceX con 'dragon'?",
    question3: "Parlami di un evento storico a cui SpaceX ha partecipato.",
    question4: "Qual Ã¨ il peso medio di una navicella spaziale?",
  },
};
