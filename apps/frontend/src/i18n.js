import LanguageDetector from "i18next-browser-languagedetector";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { LANGEN } from "./lang/en";
import { LANGFR } from "./lang/fr";
import { LANGIT } from "./lang/it";
import { LANGES } from "./lang/es";

export const resources = {
  en: {
    translation: LANGEN,
  },
  fr: {
    translation: LANGFR,
  },
  it: {
    translation: LANGIT,
  },
  es: {
    translation: LANGES,
  },
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
