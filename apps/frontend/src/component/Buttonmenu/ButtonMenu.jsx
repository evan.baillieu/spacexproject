import Menu from '../../assets/Menu.svg?react'
import './ButtonMenu.scss'

export function ButtonMenu({ onClick }) {
    return (
        <div onClick={onClick} className='ButtonMenu'>
            <Menu />
        </div>
    )
}
