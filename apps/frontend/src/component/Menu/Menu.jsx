import { useTranslation } from "react-i18next"
import { v4 as uuidv4 } from 'uuid';
import CloseButton from '../../assets/CloseButton.svg?react'
import './Menu.scss'
import { LANGFR } from "../../lang/fr";

export function Menu({ onSend, onClose }) {
    const { t } = useTranslation();

    const data = Object.keys(LANGFR.menu).filter(value => value != 'title');

    const sendMessage = (value) => {
        onSend(value);
        onClose();
    }
    return (
        <div className="menu">
            <div className="menu__title">
                <h4>{t("menu.title")}</h4>
                <div className="menu__title__closeButton" onClick={onClose}>
                    <CloseButton />
                </div>
            </div>
            <div className="menu__questions">
                {data && data.map(value => (
                    <div onClick={() => sendMessage(t(`menu.${value}`))} className="menu__questions__item" key={uuidv4()}>
                        <div className="menu__questions__item--rect">

                        </div>
                        <p>
                            {t(`menu.${value}`)}
                        </p>
                    </div>
                ))}
            </div>
        </div>
    )
}
