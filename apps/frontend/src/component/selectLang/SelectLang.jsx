import { useTranslation } from "react-i18next"
import { v4 as uuidv4 } from 'uuid';
import { resources } from "../../i18n"
import './SelectLang.scss'

export function SelectLang({ onClose }) {
    const listLang = Object.keys(resources)
    const { t, i18n } = useTranslation();

    const onChangeLang = (e) => {
        const lang = e.target.value;
        i18n.changeLanguage(lang);
        onClose();
    }

    return (
        <div data-testid="select-lang" className="selectLang">
            {listLang && listLang.map(value => (
                <div data-testid="select-lang-items" className="selectLang-item" key={uuidv4()}>
                    <input type="radio" name={value} id={`lang-${value}`} value={value} checked={i18n.language === value} onChange={onChangeLang} />
                    <label htmlFor={`lang-${value}`}>{t(`input.lang.${value}`)}</label>
                </div>
            ))}
        </div>
    )
}
