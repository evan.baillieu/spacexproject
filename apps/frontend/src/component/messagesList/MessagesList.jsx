import { v4 as uuidv4 } from 'uuid';
import './MessagesList.scss'
import { useEffect } from 'react';
export function MessagesList({ messages }) {

    useEffect(() => {
        const scrollElem = document.getElementById("zoneMessage");
        scrollElem.scrollTop = scrollElem.scrollHeight;
    }, [messages])

    return (
        <ul className='messageslist'>
            {messages && messages.map(value => (
                <li data-testid="message" className={`messageslist__message messageslist__message--${value.isYou ? 'right' : 'left'}`} key={uuidv4()}>
                    <div>
                        <p>{value.message}</p>
                    </div>
                </li>
            ))}
        </ul>
    )
}
