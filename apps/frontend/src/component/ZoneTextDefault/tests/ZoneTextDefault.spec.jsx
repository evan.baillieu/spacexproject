import { render, screen } from "@testing-library/react";
import { beforeEach, describe, expect, test } from "vitest";
import { I18nextProvider } from "react-i18next";
import { ZoneTextDefault } from "../ZoneTextDefault";
import i18n from "../../../i18n";
import { LANGEN } from "../../../lang/en";

describe("<ZoneTextDefault />", () => {
    beforeEach(() => {
        render(
            <I18nextProvider i18n={i18n}>
                <ZoneTextDefault />
            </I18nextProvider>);
    })

    test("should be title is visible", async () => {
        expect(await screen.findByText(LANGEN.message.title)).toBeVisible()
    })

    test("should be pargraphe1 is visible", async () => {
        expect(await screen.findByText(LANGEN.message.pargraphe1)).toBeVisible()
    })

    test("should be pargraphe2 is visible", async () => {
        expect(await screen.findByText(LANGEN.message.pargraphe2)).toBeVisible()
    })

    test("should be pargraphe3 is visible", async () => {
        expect(await screen.findByText(LANGEN.message.pargraphe3)).toBeVisible()
    })

    test("should be pargraphe4 is visible", async () => {
        expect(await screen.findByText(LANGEN.message.pargraphe4)).toBeVisible()
    })
})