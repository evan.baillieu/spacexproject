import { beforeEach, describe, expect, test, vi } from "vitest";
import { InputChat } from "../inputChat";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { LANGEN } from "../../../lang/en";

describe("<InputChat />", () => {
    const onSend = vi.fn();
    const user = userEvent.setup();

    beforeEach(async () => {
        render(
            <I18nextProvider i18n={i18n}>
                <InputChat
                    onSend={onSend}
                />
            </I18nextProvider>
        );
    })

    test("should be check button reglage", async () => {
        const buttonReglage = await screen.getByTestId("reglage");

        expect(buttonReglage.firstChild.nodeName).toBe('svg');

        await user.click(await screen.getByTestId("reglage"));

        const selectLang = await screen.getByTestId("select-lang");

        expect(selectLang).toBeVisible();

    })

    test("should be check button send", async () => {
        const buttonSend = await screen.getByTestId("send");
        const inputText = await screen.getByTestId("input");

        await user.type(inputText, 'enter value')
        await user.click(buttonSend);

        expect(onSend).toHaveBeenCalledTimes(1);

        expect(buttonSend.firstChild.nodeName).toBe('svg');
    })

    test("should be check input enter text", async () => {
        const inputText = await screen.getByTestId("input");

        expect(inputText.placeholder).toBe(LANGEN.input.placeholder)

        expect(inputText.value).not.toBe("enter value")

        await user.type(inputText, 'enter value')

        expect(inputText.value).toBe("enter value")
    })


})