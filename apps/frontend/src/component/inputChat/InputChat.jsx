import { useTranslation } from "react-i18next";
import { useState } from "react";
import ReglageIcon from "../../assets/reglage.svg?react";
import SendIcon from "../../assets/Send.svg?react"
import './inputChat.scss'
import { SelectLang } from "../selectLang/SelectLang";

export function InputChat({ onSend }) {
    const { t } = useTranslation();
    const [messageInput, setMessageInput] = useState("")
    const [isOpenSetting, setIsOpenSetting] = useState(false)

    const sendMessage = () => {
        if (messageInput.length > 4) {
            onSend(messageInput);

            setMessageInput("");
        }
    }

    const onChangeText = (e) => {
        const value = e.target.value

        setMessageInput(value);
    }

    const openSettingLang = () => {
        setIsOpenSetting(state => !state)
    }

    const closeSettingLang = () => {
        setIsOpenSetting(false)
    }

    return (
        <div className="inputChat">
            {isOpenSetting ? <SelectLang onClose={closeSettingLang} /> : null}
            <button data-testid="reglage" onClick={openSettingLang} className="inputChat__reglage">
                <ReglageIcon />
            </button>
            <input onChange={onChangeText} data-testid="input" type="text" placeholder={t('input.placeholder')} value={messageInput} />
            <button data-testid="send" onClick={sendMessage} className="inputChat__send">
                <SendIcon />
            </button>
        </div>
    )
}
