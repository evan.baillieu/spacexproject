export const sendMessageApi = async ({ message, idConversation, lang }) => {
  const data = await fetch(`http://localhost:8000/message/?lang=${lang}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      idConversation: idConversation || null,
      message,
    }),
  });

  return await data.json();
};
