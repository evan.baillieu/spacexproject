<?php

require_once __DIR__ . '\..\..\src\models\PDOSpaceX.php';
use PHPUnit\Framework\TestCase;

/**
 * Class tests_pdospacex
 */
class tests_pdospacex extends TestCase
{
	/**
     * @var PDOSpaceX The instance of the PDOSpaceX class for testing.
     */
    private $pdoSpaceX;

	/**
     * Set up the test environment.
     */
    protected function setUp(): void
    {
        $this->pdoSpaceX = PDOSpaceX::getPDOSpaceX();
    }

	/**
     * Test the retrieval of all messages for a given conversation.
     */
    public function testGetAllConversationMessages()
    {
        $conversationId = 2;

        $result = $this->pdoSpaceX->getAllMessages($conversationId);
        $this->assertIsArray($result);

		$message = $result[0];
    	$this->assertArrayHasKey('id_message', $message);
    	$this->assertArrayHasKey('message_time', $message);
    	$this->assertArrayHasKey('message', $message);
    	$this->assertArrayHasKey('is_bot_message', $message);

		$this->assertEquals('Hi there from UserB', $message['message']);
    	$this->assertEquals(0, $message['is_bot_message']);
    }

	/**
     * Test adding a message to a conversation.
	 * 
     * @param int $conversationId The ID of the conversation.
     */
    public function testAddMessage($conversationId = 2)
    {
        $conversationId = $conversationId;
        $message = 'Test message';
        $isBotMessage = false;

        $this->pdoSpaceX->addMessage($conversationId, $message, $isBotMessage);
        $messages = $this->pdoSpaceX->getAllMessages($conversationId);
    	$lastMessage = end($messages);

    	$this->assertNotNull($lastMessage);
    	$this->assertEquals($message, $lastMessage['message']);
    	$this->assertEquals(0, $lastMessage['is_bot_message']);
    }

	/**
     * Test creating a conversation.
     */
	public function testCreateConversation()
    {
        $title = 'Test conversation creation';
        $conversationId = $this->pdoSpaceX->createConversation($title);
        $this->assertGreaterThan(0, $conversationId);
    }

	/**
     * Test clearing messages from a conversation.
     */
    public function testClearMessages()
    {
		$title = "Conversation that will be deleted";
		$conversationId = $this->pdoSpaceX->createConversation($title);
		$this->pdoSpaceX->addMessage($conversationId, 'Test message', false);

        $this->pdoSpaceX->clearMessages($conversationId);

		$messagesAfterClear = $this->pdoSpaceX->getAllMessages($conversationId);
    	$this->assertEmpty($messagesAfterClear);
    }
}
