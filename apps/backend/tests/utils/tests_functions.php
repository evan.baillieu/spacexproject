<?php

require_once __DIR__ . '\..\..\src\utils\functions.php';
use PHPUnit\Framework\TestCase;

/**
 * PHPUnit test class for the SpaceX functions.
 */
class tests_functions extends TestCase
{
	// SpaceX API URL address
    private $testApiUrl = 'https://api.spacexdata.com/v3/dragons/dragon1';

	/**
     * Test the getSpaceXAPIData function.
     */
    public function testGetSpaceXAPIData()
    {
        $result = getSpaceXAPIData($this->testApiUrl);

        $this->assertIsArray($result);
		$this->assertNotEquals('No API response', $result);
    }

	/**
     * Test the getChatGPTAnswer function.
	 * 
	 * Ensures that the result from getChatGPTAnswer is a valid chatGPT response 
 	 * in the form of a string and does not exceed a length of 5000 characters.
 	 */
    public function testGetChatGPTAnswer()
    {
        $userInput = 'Hello, GPT-3.5 Turbo!';
        $result = getChatGPTAnswer($userInput);

        $this->assertIsString($result);
		$this->assertNotEquals('No answer to that.', $result);
		// maximum size for a message is 5000
		$this->assertLessThanOrEqual(5000, strlen($result));
    }
}
