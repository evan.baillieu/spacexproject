<?php

require_once __DIR__ . '\..\..\src\controllers\spacex_controller.php';
require_once __DIR__ . '\..\..\src\models\PDOSpaceX.php';

use PHPUnit\Framework\TestCase;

class tests_spacexcontroller extends TestCase
{
	private $pdoSpaceX;
    private $controller;

	public function setUp(): void
    {
        $this->pdoSpaceX = PDOSpaceX::getPDOSpaceX();
        $this->controller = new SpaceXController($this->pdoSpaceX);
    }
	
    public function testInitialiseConversation()
    {
		$initialPrompt = 'Test initial prompt';
        $conversationId = $this->controller->initialise_conversation($initialPrompt);

        $this->assertGreaterThan(0,  $conversationId);

		$pdo = $this->pdoSpaceX->getPDO();
		$query = $pdo->prepare(
			"SELECT id_conversation FROM CONVERSATION WHERE id_conversation = :id_conv"
		);
    	$query->bindParam(':id_conv', $conversationId, PDO::PARAM_INT);
    	$query->execute();
    	$result = $query->fetch(PDO::FETCH_ASSOC);

    	$this->assertEquals($conversationId, $result['id_conversation']);
    }

    public function testProcessUserInput()
    {
        $userText = 'Tell me 10 words about space';
        $response = $this->controller->processUserInput($userText);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);

		$response = $this->controller->processUserInput($userText, 5);

		$this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);
		$this->assertEquals(5, $response['id_conv']);
    }

    public function testProcessInfosSpaceX()
    {
        $response = $this->controller->processInfosSpaceX();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);

		$response = $this->controller->processInfosSpaceX(5);

		$this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);
		$this->assertEquals(5, $response['id_conv']);
    }

    public function testProcessDragonSpaceX()
    {
        $response = $this->controller->processDragonSpaceX();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);

		$response = $this->controller->processDragonSpaceX(5);

		$this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);
		$this->assertEquals(5, $response['id_conv']);
    }

    public function testProcessSpaceXHistory()
    {
        $response = $this->controller->processSpaceXHistory();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);

		$response = $this->controller->processSpaceXHistory(5);

		$this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);
		$this->assertEquals(5, $response['id_conv']);
    }

    public function testProcessShipsAverageWeigh()
    {
        $response = $this->controller->processShipsAverageWeigh();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);

		$response = $this->controller->processShipsAverageWeigh(5);

		$this->assertIsArray($response);
        $this->assertArrayHasKey('message', $response);
        $this->assertArrayHasKey('id_conv', $response);
		$this->assertEquals(5, $response['id_conv']);
    }
}
