<?php

require_once __DIR__ . '/../utils/functions.php';

/**
 * SpaceXController Class
 *
 * This class handles the communication between the user, the SpaceX API,
 * and the GPT model for generating responses.
 */
class SpaceXController {
	private $pdo;
	private $answerRequest, $titleRequest, $extraInstruction;

    public function __construct($pdo) {
        $this->pdo = $pdo;
		$this->answerRequest = "\n Write a text based on the JSON in a professional and informative tone, 
						  		which will be used as a description on a website. No validation phrases.";
		$this->titleRequest = "\n Summarize this text into a title for a conversation.";
		$this->extraInstruction = "Your message can never be longer than 300 caracteres. Answer the message in the {} language";
	}

	private function getLang($lang){
		switch($lang){
      		case "fr": 
				return "French";
      		case "en":
				return "English";
      		case "it": 
				return "Italian";
      		case "es": 
				return "Spanish";
			default:
				return "English";
    	};
	}

	/**
	* Initialize conversation if not set.
	*
	* @param string $first_prompt The initial prompt for starting a conversation.
	*/
	public function initialise_conversation($first_prompt) {
		$title = getChatGPTAnswer($first_prompt . $this->titleRequest);
		$id_conv = $this->pdo->createConversation($title);
		return $id_conv;
	}

	/**
 	* Process user input, interact with the GPT model, and store messages in the database.
 	*
 	* @param string $userText User input text.
 	* @return string Bot-generated answer.
 	*/
    public function processUserInput($userText, $id_conv=null) {
		if (!$id_conv)
			$id_conv = $this->initialise_conversation($userText);

		$this->pdo->addMessage($id_conv, $userText, 0);
        $botAnswer = getChatGPTAnswer($userText . str_ireplace("{}", $this->getLang(), $this->extraInstruction));
		$this->pdo->addMessage($id_conv, $botAnswer, 1);

		$response = array(
			"message" => $botAnswer,
			"id_conv" => $id_conv
		);

		return $response;
    }

	/**
 	* Fetch and process information about the SpaceX company.
 	*
 	* @return string Processed information about SpaceX.
 	*/
    public function processInfosSpaceX($id_conv=null, $lang='en') {
        $api_url = 'https://api.spacexdata.com/v3/info';

		$userQuestion = 'Give me informations about the SpaceX company.';
		if (!$id_conv)
			$id_conv = $this->initialise_conversation($userQuestion);
		$this->pdo->addMessage($id_conv, $userQuestion, 0);

		$infos_data = getSpaceXAPIData($api_url);
		$processedInformations = getChatGPTAnswer(json_encode($infos_data) . $this->answerRequest . str_ireplace("{}", $this->getLang($lang), $this->extraInstruction));
		$this->pdo->addMessage($id_conv, $processedInformations, 1);

		$response = array(
			"message" => $processedInformations,
			"id_conv" => $id_conv
		);
		return $response;
    }

	/**
 	* Fetch and process information about the SpaceX Dragon.
 	*
 	* @return string Processed information about SpaceX Dragon.
 	*/
    public function processDragonSpaceX($id_conv=null, $lang='en') {
        $api_url = 'https://api.spacexdata.com/v3/dragons/dragon1';

		$userQuestion = 'What does SpaceX mean by dragon?';
		if (!$id_conv)
			$id_conv = $this->initialise_conversation($userQuestion);
		$this->pdo->addMessage($id_conv, $userQuestion, 0);

		$infos_data = getSpaceXAPIData($api_url);
		$processedInformations = getChatGPTAnswer(json_encode($infos_data) . $this->answerRequest . str_ireplace("{}", $this->getLang($lang), $this->extraInstruction));
		$this->pdo->addMessage($id_conv, $processedInformations, 1);

		$response = array(
			"message" => $processedInformations,
			"id_conv" => $id_conv
		);
		return $response;
    }

	/**
 	* Fetch and process a random SpaceX history from the SpaceX API.
 	*
 	* @return string Processed information about SpaceX history.
 	*/
    public function processSpaceXHistory($id_conv=null, $lang='en') {
		$randomNumber = rand(1, 20);
        $api_url = 'https://api.spacexdata.com/v3/history/' . $randomNumber;

		$userQuestion = 'Tell me about one historical event SpaceX was involved in.';
		if (!$id_conv)
			$id_conv = $this->initialise_conversation($userQuestion);
		$this->pdo->addMessage($id_conv, $userQuestion, 0);

		$infos_data = getSpaceXAPIData($api_url);
		$processedInformations = getChatGPTAnswer(json_encode($infos_data) . $this->answerRequest . str_ireplace("{}", $this->getLang($lang), $this->extraInstruction));
		$this->pdo->addMessage($id_conv, $processedInformations, 1);

		$response = array(
			"message" => $processedInformations,
			"id_conv" => $id_conv
		);
		return $response;
	}

	/**
 	* Fetch and return the average weight of a SpaceX ship.
 	*
 	* @return string Processed information about SpaceX ship weight.
 	*/
    public function processShipsAverageWeigh($id_conv=null) {
        $api_url = 'https://api.spacexdata.com/v3/ships';

		$userQuestion = 'What is the average weight of a ship?';
		if (!$id_conv)
			$id_conv = $this->initialise_conversation($userQuestion);
		$this->pdo->addMessage($id_conv, $userQuestion, 0);

		$ships_data = getSpaceXAPIData($api_url);

		// Calculate the average weight of SpaceX ships based on available data.
		$ite = 0;
		$weights = 0;
		foreach ($ships_data as $ship) {
			if (isset($ship['weight_kg'])) {
				$weights += $ship['weight_kg'];
				$ite += 1;
			}
		}
		$weight = intval($weights / $ite);
		$processedInformations = 'The average weight of a SpaceX ship is ' . $weight . 'kg.';
		$this->pdo->addMessage($id_conv, $processedInformations, 1);

		$response = array(
			"message" => $processedInformations,
			"id_conv" => $id_conv
		);
		return $response;
	}
}
?>
