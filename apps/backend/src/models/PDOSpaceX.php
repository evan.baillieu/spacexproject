<?php
/** Data access class
 *  -------
 *  @file
 *  @brief Use the PDO class services for the SpaceXProject application
 * 
 *  @package   spacexproject
 */
 class PDOSpaceX
{
    private static $server = 'mysql:host=127.0.0.1';
    private static $db = 'dbname=spacexproject';
    private static $user = 'root2';
    private static $pwd = 'rootpass';
    private static $myPdo;
    private static $myPDOSpaceX = null;

    /**
	 * Private constructor, create a PDO instance 
     */
    private function __construct()
    {
        PDOSpaceX::$myPdo = new PDO(
            PDOSpaceX::$server . ';' . PDOSpaceX::$db,
            PDOSpaceX::$user,
            PDOSpaceX::$pwd
        );
        PDOSpaceX::$myPdo->query('SET CHARACTER SET utf8');
    }

    /**
	 * Destructor method for the PDO object
     */
    public function __destruct()
    {
        PDOSpaceX::$myPdo = null;
    }

	/**
     * get on the myPdo attribute
     *
     * @return myPdo Attribute PDO
     */
	public function getPDO()
    {
        return self::$myPdo;
    }

    /**
	 * Static function that create the unique instance of the class
     *
     * @return unique object of the PDOSpaceX class
     */
    public static function getPDOSpaceX()
    {
        if (PDOSpaceX::$myPDOSpaceX == null) {
            PDOSpaceX::$myPDOSpaceX = new PDOSpaceX();
        }
        return PDOSpaceX::$myPDOSpaceX;
    }

	/**
     * Get all messages for a given conversation
     *
     * @param int $conversationId Conversation ID
     *
     * @return array Array of messages
     */
    public function getAllMessages($conversationId)
	{
        $query = PDOSpaceX::$myPdo->prepare(
			'SELECT * '
			. 'FROM MESSAGE '
			. 'WHERE id_conversation = :id_conversation'
		);
        $query->bindParam(':id_conversation', $conversationId, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

	/**
     * Add a message to the MESSAGE table
     *
     * @param int    $conversationId Conversation ID
     * @param string $message        Message content
     * @param bool   $isBotMessage   Is the message from a bot
     */
    public function addMessage($conversationId, $message, $isBotMessage)
	{
		$query = PDOSpaceX::$myPdo->prepare(
			'INSERT INTO MESSAGE (id_conversation, message_time, message, is_bot_message) '
			. 'VALUES (:id_conversation, CURRENT_TIMESTAMP, :message, :is_bot_message)'
		);
		$query->bindParam(':id_conversation', $conversationId, PDO::PARAM_INT);
		$query->bindParam(':message', $message, PDO::PARAM_STR);
		$query->bindParam(':is_bot_message', $isBotMessage, PDO::PARAM_BOOL);
		$query->execute();
	}

	/**
	 * Clear all messages from a given conversation
	 *
	 * @param int $conversationId Conversation ID
	 */
	public function clearMessages($conversationId)
	{
	    $query = PDOSpaceX::$myPdo->prepare(
			'DELETE FROM MESSAGE '
			. 'WHERE id_conversation = :id_conversation'
		);
	    $query->bindParam(':id_conversation', $conversationId, PDO::PARAM_INT);
	    $query->execute();

		$query = PDOSpaceX::$myPdo->prepare(
			'DELETE FROM CONVERSATION '
			. 'WHERE id_conversation = :id_conversation'
		);
	    $query->bindParam(':id_conversation', $conversationId, PDO::PARAM_INT);
	    $query->execute();
	}

	/**
	 * Create a new conversation
	 *
	 * @param string $title Conversation title
	 */
	public function createConversation($title) {
        try {
            $query = PDOSpaceX::$myPdo->prepare(
				'INSERT INTO conversation (title) '
				. 'VALUES (:title)'
			);
			$query->bindParam(':title', $title, PDO::PARAM_STR);
            $query->execute();

			$conversationId = PDOSpaceX::$myPdo->lastInsertId('id_conversation');
            return $conversationId;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
