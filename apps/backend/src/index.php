<?php

require_once('./src/controllers/spacex_controller.php');
require_once('./src/models/PDOSpaceX.php');


// Autoriser toutes les origines à accéder à votre API
header('Access-Control-Allow-Origin: *');

// Format des données envoyées
header("Content-Type: application/json; charset=UTF-8");

// Méthodes autorisées
header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE");

// Durée de vie de la requête (en secondes)
header("Access-Control-Max-Age: 3600");

// Entêtes autorisées
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('HTTP/1.1 200 OK');
    exit();
}

$method = $_SERVER['REQUEST_METHOD'];

$spaceXController = new SpaceXController(PDOSpaceX::getPDOSpaceX());

switch ($method) {
 
 case 'POST':
    // Create operation (add a new book)
    $data = json_decode(file_get_contents('php://input'), true);
    
    if(isset($_GET["lang"])){
        $lang = $_GET["lang"];
    } else{
        $lang = 'en';
    }


    if(isset($data['idConversation'])){
        $idConvertion = $data['idConversation'];
    } else{
        $idConvertion = null;
    }

    $message = $data['message'];
    switch ($message) {
        case "Dammi informazioni sull'azienda SpaceX.":
        case "Give me informations about the SpaceX company.":
        case "Dame información sobre la empresa SpaceX.":
        case "Donnez-moi des informations sur l'entreprise SpaceX.":
            $reponse = $spaceXController->processInfosSpaceX($idConvertion, $lang);
            break;
        case "Que veut dire SpaceX par Dragon ?":
        case "What does SpaceX mean by dragon?":
        case "¿Qué significa SpaceX con respecto a 'dragon'?":
        case "Cosa significa SpaceX con 'dragon'?":
            $reponse = $spaceXController->processDragonSpaceX($idConvertion, $lang);
            break;
        case "Parlez-moi d'un événement historique auquel SpaceX a participé.":
        case "Tell me about one historical event SpaceX was involved in.":
        case "Háblame sobre un evento histórico en el que SpaceX estuvo involucrado.":
        case "¿Cuál es el peso promedio de una nave espacial?":
            $reponse = $spaceXController->processSpaceXHistory($idConvertion, $lang);
            break;
        case "Quel est le poids moyen d'un vaisseau spatial ?":
        case "What is the average weight of a ship?":
        case "Qual Ã¨ il peso medio di una navicella spaziale?":
        case "¿Cuál es el peso promedio de una nave espacial?":
            $reponse = $spaceXController->processShipsAverageWeigh($idConvertion);
            break;
        
        default:
            $reponse = array([
                "idConversation" => $idConvertion,
                "message" => "message.default"
            ]);
            break;
    }
    http_response_code(200);
    echo json_encode($reponse);
    break;
 default:
 // Invalid method
 http_response_code(405);
 echo json_encode(['error' => 'Method not allowed']);
 break;
}