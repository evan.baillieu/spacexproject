<?php
// config.php
require_once __DIR__ . '../../../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../../');
$dotenv->load();

define('CACERT_PATH', __DIR__ . '/cacert.pem');
define('OPENAI_API_KEY', $_ENV['OPENAI_API_KEY']);
?>
