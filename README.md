# SpaceXChatbot Project

La conquête spatiale est à l'avant-garde des avancées technologiques, avec SpaceX menant la révolution sous la direction d'Elon Musk. Ce projet vise à évaluer les compétences en conception logicielle, IA, et gestion de projet à travers l'étude de cas de SpaceX.

## Objectif du Projet

Concevoir et développer un chatbot intégré au site de SpaceX (https://www.spacex.com). Notre chatbot permettra aux utilisateurs d'interagir avec un assistant intelligent, recevant des réponses automatiques basées sur leurs choix. De plus, le chatbot exploitera une API externe spécialisée dans les réponses basées sur l'intelligence artificielle.

## Approche Innovante

La créativité est mise en avant en proposant un concept de chatbot innovant qui explore des idées novatrices tout en respectant le cadre imposé par SpaceX.

---

